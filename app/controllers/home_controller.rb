class HomeController < ApplicationController
	layout 'cover', only: [:index, :comenzar]
	before_filter :check_user

	def index; end
	def comenzar; end

	private
	def check_user
		if apoderado_signed_in?
			redirect_to home_apoderado_path
		elsif profesor_signed_in?
			redirect_to home_profesor_path
		elsif alumno_signed_in?
			redirect_to home_alumnos_path
		end
	end
end

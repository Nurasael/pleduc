class Profesors::CursosController <  ActionController::Base
    before_filter :authenticate_profesor!
    before_filter :set_curso, only: [:update]
    # home controller apoderados
    layout 'layout'
  
	def index
		@cursos = Curso.all
	end

	def edit
		@curso = Curso.find(params[:id])
	end

	def alumnos
		@curso = Curso.find(params[:id])
		@alumnos = @curso.alumnos
	end

	def add
		
		if Alumno.where(rut: params[:alumno][:rut]).exists?
			@alumno = Alumno.where(rut: params[:alumno][:rut]).first
			@alumno.curso_id = params[:alumno][:curso_id]
			if @alumno.save
				redirect_to alumnos_cursos_profesor_path(params[:alumno][:curso_id]), notice: "Alumno agregado."
			end
		else
			redirect_to alumnos_cursos_profesor_path(params[:alumno][:curso_id]), alert: "Alumno no existe, no se pudo agregar."
		end
	end


	def delete_alumno
		@alumno = Alumno.find(params[:alumno_id])
		@alumno.curso_id = nil
		if @alumno.save
			redirect_to alumnos_cursos_profesor_path(params[:curso_id]), notice: 'Alumno eliminado del curso'
		else
			redirect_to alumnos_cursos_profesor_path(params[:curso_id]), alert: 'Alumno no ha sido eliminado del curso'
		end
	end

	# GET /cursos/new
  def new
    @curso = Curso.new
  end

  # POST /cursos
  def create
    @curso = Curso.new(curso_params)

    if @curso.save
      redirect_to cursos_profesor_path, notice: 'Curso agregado.'
    else
      render :new
    end
  end

  # PATCH/PUT /cursos/1
  def update
    if @curso.update(curso_params)
      redirect_to cursos_profesor_path, notice: 'Curso se ha actualizado.'
    else
      render :edit
    end
  end

  # DELETE /cursos/1
  def destroy
  	@curso = Curso.find(params[:id])
    @curso.destroy
    redirect_to cursos_profesor_path, notice: 'Curso eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_curso
      @curso = Curso.find(params[:curso][:curso_id])
    end

    # Only allow a trusted parameter "white list" through.
    def curso_params
      params.require(:curso).permit(:nombre, :sigla, :profesor_id)
    end

	# Use callbacks to share common setup or constraints between actions.
	def set_alumno
	  @alumno = current_alumno
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def alumno_params
	  params.require(:alumno).permit(:nombres, :apellidos, :fecha_de_nacimiento, :nivel, :curso, :email, :password, :password_confirmation)
	end
end
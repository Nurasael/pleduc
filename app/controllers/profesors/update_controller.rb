class Profesors::UpdateController <  ActionController::Base
  before_filter :authenticate_profesor!
  before_action :set_profesor, only: [:info, :update]
  
  layout 'layout'
  
  def info
  end

  def update
  	if @profesor.update(profesor_params)
  		redirect_to home_profesor_path
  	else
  		render :info, notice: 'No se pudo actualizar'
  	end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profesor
      @profesor = current_profesor
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profesor_params
      params.require(:profesor).permit(:nombre, :apellido, :rut, :email, :password, :password_confirmation)
    end
end
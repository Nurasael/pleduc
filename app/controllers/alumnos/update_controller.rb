class Alumnos::UpdateController <  ActionController::Base
  before_filter :authenticate_alumno!
  before_action :set_alumno, only: [:info, :update]

  layout 'layout'
  
  def info
  end

  def update
  	if @alumno.update(alumno_params)
  		redirect_to home_alumnos_path
  	else
  		render :info, notice: 'No se pudo actualizar'
  	end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_alumno
      @alumno = current_alumno
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def alumno_params
      params.require(:alumno).permit(:nombres, :apellidos, :fecha_de_nacimiento, :nivel, :curso, :email, :password, :password_confirmation)
    end
end
class Alumnos::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters
 
  protected
    # custom fields
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit(:nombres, :apellidos, :fecha_de_nacimiento, :rut, :nivel, :curso, :email, :password, :password_confirmation)
      end
      devise_parameter_sanitizer.for(:account_update) do |u|
        u.permit(:nombres, :apellidos, :fecha_de_nacimiento, :rut, :nivel, :curso, :email, :password, :password_confirmation, :current_password)
      end
    end
end
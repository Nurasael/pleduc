class Apoderados::UpdateController <  ActionController::Base
  before_filter :authenticate_apoderado!
  before_action :set_apoderado, only: [:info, :update]
  
  layout 'layout'
  
  def info
  end

  def update
  	if @apoderado.update(apoderado_params)
  		redirect_to home_apoderado_path
  	else
  		render :info, notice: 'No se pudo actualizar'
  	end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_apoderado
      @apoderado = current_apoderado
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def apoderado_params
      params.require(:apoderado).permit(:nombre, :apellido, :run, :email, :password, :password_confirmation)
    end
end
class Apoderados::HomeController <  ActionController::Base
  before_filter :authenticate_apoderado!
  # home controller apoderados
  layout 'layout'
  def index
  end
end
class Cursos::CursosController < ApplicationController
  before_action :set_curso, only: [:index]
  layout 'layout'
  # GET /cursos
  def index
  	@noticias = Noticias.all.where(curso_id: @curso.id).order('created_at DESC')
  end

  def desc
  	@curso = Curso.find(params[:id])
  end

  def update
  	@curso = Curso.find(params[:curso][:curso_id])
  	if @curso.update(curso_params)
  		redirect_to curso_path(@curso), notice: 'Descripcion actualizada'
  	end
  end

  def upload
    @file = Upload.new(upload_params)
    @file.curso_id = params[:id]
    if @file.save
      redirect_to curso_path(@file.curso_id), notice: 'Material subido exitosamente.'
    else
      redirect_to curso_path(@file.curso_id), notice: 'Material subido exitosamente.'
    end
  end

  private
    def set_curso
      @curso = Curso.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def curso_params
      params.require(:curso).permit(:desc)
    end

    def upload_params
      params.require(:upload).permit(:titulo, :file)
    end
end

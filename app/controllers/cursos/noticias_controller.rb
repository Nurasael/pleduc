class Cursos::NoticiasController < ApplicationController
  layout 'layout'

  def new
    @curso = Curso.find(params[:id])
  end


  def create
    @noticia = Noticias.new(noticias_params)
    if @noticia.save
      redirect_to curso_path(@noticia.curso_id), notice: 'Noticia agregada'
    else
      redirect_to curso_path(@noticia.curso_id), alert: 'No se puduo agregar la noticia'
    end
  end


  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def noticias_params
      params.require(:noticias).permit(:titulo, :contenido, :curso_id)
    end

end

class Alumno < ActiveRecord::Base
	before_save :titleize_alumno
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise 	:database_authenticatable, :registerable,
			:recoverable, :rememberable, :trackable, :validatable

	belongs_to :apoderado
	belongs_to :curso

	def titleize_alumno
		self.nombres = self.nombres.titleize
		self.apellidos = self.apellidos.titleize
	end
end

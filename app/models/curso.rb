class Curso < ActiveRecord::Base
	belongs_to :profesor

	has_many :alumnos
	has_many :noticias
  has_many :uploads
end

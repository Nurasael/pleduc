class Upload < ActiveRecord::Base
  belongs_to :curso

  #specify that the resume is a paperclip file attachment
  has_attached_file :file
  do_not_validate_attachment_file_type :file
end

json.array!(@alumnos) do |alumno|
  json.extract! alumno, :id, :nombres, :apellidos, :fecha_de_nacimiento, :nivel, :curso, :email
  json.url alumno_url(alumno, format: :json)
end

Pleduc::Application.routes.draw do

  	# devise alumnos
  	devise_for :alumnos, :controllers => {:registrations => "alumnos/registrations"} 
	
	# devise profesores
  	devise_for :profesors, :controllers => {:registrations => "profesors/registrations"}
  	
  	# devise apoderados
  	devise_for :apoderados, :controllers => {:registrations => "apoderados/registrations"} 
	
  	# default route
  	root 'home#index'
  	# comenzar
  	get '/comenzar' => 'home#comenzar', as: 'comenzar'

  	# ======================================================================================== 	
  	# rutas profesores
  	# ========================================================================================
  	get '/profesores' => "profesors/home#index", as: 'home_profesor'
  	get '/profesores/update' => 'profesors/update#info', as: 'update_info_profesor'
  	patch '/profesores/update' => 'profesors/update#update', as: 'update_info_form_profesor'

    # cursos profesor
    get '/profesores/cursos' => 'profesors/cursos#index', as: 'cursos_profesor'
    get '/profesores/cursos/:id/edit' => 'profesors/cursos#edit', as: 'edit_cursos_profesor'
    get '/profesores/cursos/:id/alumnos' => 'profesors/cursos#alumnos', as: 'alumnos_cursos_profesor'
    post '/profesores/cursos/:id/alumnos/nuevo' => 'profesors/cursos#add', as: 'add_alumnos_cursos_profesor'
    get '/profesores/cursos/:curso_id/alumno/:alumno_id/delete' => 'profesors/cursos#delete_alumno', as: 'del_alumnos_cursos_profesor'

    get '/profesores/cursos/nuevo' => 'profesors/cursos#new', as: 'create_cursos_profesor'
    post '/profesors/cursos/create' => 'profesors/cursos#create', as: 'post_cursos_profesor'
    patch '/profesors/cursos/update' => 'profesors/cursos#update', as: 'update_cursos_profesor'
    get '/profesors/cursos/:id/destroy' => 'profesors/cursos#destroy', as: 'destroy_cursos_profesor'

  	# ========================================================================================
  	# rutas apoderados
  	# ========================================================================================
  	get '/apoderados' => 'apoderados/home#index', as: 'home_apoderado'
  	get '/apoderados/update' => 'apoderados/update#info', as: 'update_info_apoderado'
  	patch '/apoderados/update' => 'apoderados/update#update', as: 'update_info_form_apoderado'

  	# ========================================================================================
  	# rutas alumnos
  	# ========================================================================================
  	get '/alumnos' => 'alumnos/home#index', as: 'home_alumnos'
  	get '/alumnos/update' => 'alumnos/update#info', as: 'update_info_alumno'
  	patch '/alumnos/update' => 'alumnos/update#update', as: 'update_info_form_alumno'

    # ========================================================================================
    # rutas cursos
    # ========================================================================================

    get '/curso/:id' => 'cursos/cursos#index', as: 'curso'
    
    get '/curso/:id/descripcion' => 'cursos/cursos#desc', as: 'desc_curso'
    patch '/curso/descripcion' => 'cursos/cursos#update', as: 'desc_update_curso'

    get '/curso/:id/noticias/nueva' => 'cursos/noticias#new', as: 'nueva_noticia_curso'
    post '/curso/noticias/' => 'cursos/noticias#create', as: 'post_noticia_curso'

    post '/curso/:id/uploads' => 'cursos/cursos#upload', as: 'curso_upload'
    get '/curso/delete/:file' => 'cursos/cursos#delete', as: 'curso_delete'
end

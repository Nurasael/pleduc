# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140616131859) do

  create_table "alumnos", force: true do |t|
    t.string   "nombres"
    t.string   "apellidos"
    t.date     "fecha_de_nacimiento"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "apoderado_id"
    t.integer  "curso_id"
    t.string   "rut"
  end

  add_index "alumnos", ["email"], name: "index_alumnos_on_email", unique: true
  add_index "alumnos", ["reset_password_token"], name: "index_alumnos_on_reset_password_token", unique: true

  create_table "apoderados", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "nombre"
    t.string   "apellido"
    t.string   "run"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "apoderados", ["email"], name: "index_apoderados_on_email", unique: true
  add_index "apoderados", ["reset_password_token"], name: "index_apoderados_on_reset_password_token", unique: true

  create_table "cursos", force: true do |t|
    t.string   "nombre"
    t.string   "sigla"
    t.integer  "profesor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
  end

  create_table "noticias", force: true do |t|
    t.string   "titulo"
    t.text     "contenido"
    t.integer  "curso_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profesors", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "nombre"
    t.string   "apellido"
    t.string   "rut"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profesors", ["email"], name: "index_profesors_on_email", unique: true
  add_index "profesors", ["reset_password_token"], name: "index_profesors_on_reset_password_token", unique: true

  create_table "uploads", force: true do |t|
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "titulo"
    t.integer  "curso_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

class AddCursoIdToAlumnos < ActiveRecord::Migration
  def change
    add_column :alumnos, :curso_id, :integer
    remove_column :alumnos, :nivel
    remove_column :alumnos, :curso
  end
end

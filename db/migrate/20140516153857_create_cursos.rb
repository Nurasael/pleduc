class CreateCursos < ActiveRecord::Migration
  def change
    create_table :cursos do |t|
      t.string :nombre
      t.string :sigla
      t.integer :profesor_id

      t.timestamps
    end
  end
end

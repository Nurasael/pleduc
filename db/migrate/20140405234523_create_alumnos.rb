class CreateAlumnos < ActiveRecord::Migration
  def change
    create_table :alumnos do |t|
      t.string :nombres
      t.string :apellidos
      t.date :fecha_de_nacimiento
      t.integer :nivel
      t.string :curso
      t.string :email

      t.timestamps
    end
  end
end

class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.attachment :file
      t.string :titulo
      t.references :curso
      t.timestamps
    end
  end
end
